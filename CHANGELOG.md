# changes by release

## 0.3.1
### fixes
* datasets_usage: increase the limit query param to 50

### other
* include and exclude option now uses regex instead of array

## 0.3.0
### features
* new modes:
  * zpool_iostat_read_iops
  * zpool_iostat_write_iops
  * zpool_iostat_read_bw
  * zpool_iostat_write_bw

### other
* updated tested versions

## 0.2.0
### features
* new modes:
 * zpool_usage
 * dataset_quota
 * arc_cache_miss_ratio
 * arc_cache_meta_usage
 * arc_cache_mru_ghost
 * arc_cache_mfu_ghost
 * io_requests_pending
 * disk_busy

### other
* no *https://* prefix necessary anymore for address
* renamed mode *datasets* to *datasets_usage*
* refactored some helper methods


## 0.1.1
### fixes
* *alert_check*: convert empty json output to OK status

### other
* catch all http responses instead of a few

## 0.1
* Initial release
